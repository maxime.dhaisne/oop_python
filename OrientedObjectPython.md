# Oriented Object in Python

We use the keyword `class` to create a class.

```python

class Vehicle:
    pass # Pass is a term to tell that nothing is defined

```

## Basic class

```python
class Vehicle(object):
    color = 'blue'
    x_position = 0.0

    def move(self):
        x_position += 5.0

# my_vehicle is an instance of Vehicle
my_vehicle = Vehicle()
```

## List of main built-in methods

- Built-in Attributes:
    - __\_\_name____ - returns a class name;
    ```python
    print(my_vehicle.__name__)
    >> Vehicle
    ```
    - __\_\_dict____ - returns a dictionary of local variables (attributes) for an object/class;

    - __\_\_slots___ - A special attribute to define only attributes to be created in an object.
    ```python
    class Car:    
        __slots__ = ('number_of_wheel', 'color')
    
        def __init__(number_of_wheel, color):
            self.number_of_wheel = number_of_wheel
            self.color = color

    c = Car(4, 'red')
    c.speed = 30.0
    >> AttributeError: 'Car' object has no attribute 'speed'
    ```

- Built-in Functions:
    - __\_\_init____() - Init is a magic method to initialize created object and not to create an object. It is NOT a constructor. In python objects are being created with the method __\_\_new____(). See more about new and init [here](https://spyhce.com/blog/understanding-new-and-init)
    - __getattr(obj, 'name')__ - returns an attribute value of an object;
    ```python
    print(getattr(my_vehicle, 'color'))
    >> blue
    # Equivalent to
    print(my_vehicle.color)
    >> blue
    ```
    - __setattr(obj, 'name', value)__ - set a new value for an attribute;
    ```python
    setattr(my_vehicle, 'color', 'red')
    print(my_vehicle.color)
    >> red
    # Equivalent to
    my_vehicle.color = 'yellow'
    print(my_vehicle.color)
    >> yellow
    ```
    - __delattr(obj, 'name')__ - deletes an attribute;
    ```python
    delattr(my_vehicle, 'color')
    print(my_vehicle.color)
    >> Error, Vehicle doesnt have color attribute
    ```
    - __hasattr(obj, 'name')__ - checks if an object has an attribute;
    ```python
    hasattr(my_vehicle, 'color')
    >> True
    ```
    - __dir(obj or a class)__ - returns a complete set of attributes for an object or a class;
    - __isinstance(obj, class)__ - checks whether an object is an instance of a certain class
    ```python
    isinstance(my_vehicle, Vehicle)
    >> True
    ```

You can find more about the magic methods [here](https://www.tutorialsteacher.com/python/magic-methods-in-python)

## General ideas and concepts

- **Attributes** are variables;
- **Methods** are functions;
- **Attributes** and **methods** are called by using the **dot notation** (e.g. `car.color` or `car.move()`);
- **self** argument is a linkage between methods and objects;
- If a method doesn't take in an object, it's a **static method**;
- **Static methods** are called by using a special decorator (9. Decorators) @static method;
- New attributes that haven't been defined in a class can be created.- However, it leads to inconsistency.

- **Constructor** is a method which is called automatically when an object is being created (e.g. __\_\_init____() );
- **Destructor** is a method which is called automatically when an object is being deleted (e.g. __\_\_del____() );
- self.name, self.weight... are attributes/fields whereas name, weight, heigh... are parameters;
- Having default parameters in any methods, make sure you follow the order: non - default parameters first, then default parameters;
- __\_\_init____() has to take in self parameter;
- **Methods with underscores** are special. They are called methods of operator overloading or magic methods;
- **Magic methods** are called automatically when an object takes part in a certain operation (e.g. addition __\_\_add____() );
- Methods with the same name **override** each other;
- Constructor allows predefining attributes of objects;
- __\_\_slots____ field allows only certain attributes to be created
- **Class attributes** are defined outside methods and can be accessed via class or object;
- **Object attributes** are defined inside methods and can be accessed only via an object;
- **Local variables/attributes** are defined in methods or code blocks;
- **Global variables/attributes** are defined outside methods or code blocks

- Child class inherits parental methods and attributes;
- Child methods can be extended by parental ones;
- Polymorphism - the same methods name but different logic;
- Attributes and methods can be encapsulated (public, protected and private);
- Protected and Public attributes are identical;
- For calling private attributes define getters and setters


## Class attributes and Object attributes

```python
class Car:
    # This attribute will belong to the class and will be shared through all Car objects
    color = 'red'

    # All attributes inside this method will belong to objects
    def __init__(name):
        self.name = name
```
- **Class attributes** can be accessed via an **object** or a **class** (when there aren't any objects yet)
- **Object attributes** can be accessed only via an **object**

## Inheritance

```python
class Vehicle:
    def __init__(number_of_wheel, color):
        self.number_of_wheel = number_of_wheel
        self.color = color

class Car(Vehicle):
    def klaxon(self):
        print('pouet pouet')

my_vehicle = Vehicle(5, 'red')
my_vehicle.klaxon()
>> Erreur Vehicle ne possede pas la methode klaxon()

my_car = Car(6, 'red')
my_car.klaxon()
>> pouet pouet
```

### Constructor extension

```python
class Vehicle:
    def __init__(self, number_of_wheel, color):
        self.number_of_wheel = number_of_wheel
        self.color = color

class Car(Vehicle):

    def __init__(self, color, motor):
        Vehicle.__init__(4, color)
        self.motor = motor

    def klaxon(self):
        print('pouet pouet')

my_vehicle = Vehicle(5, 'red')
my_vehicle.klaxon()
>> Erreur Vehicle ne possede pas la methode klaxon()

my_car = Car(6, 'red')
my_car.klaxon()
>> pouet pouet
```

## Access modifiers

```python
class Vehicle:
    # Public
    color = 'red'
    # Protected
    _brand = 'citroen'
    ## Private
    __speed = '100.0'
```

## Extension and override

### Extension
```python
def speed(self):
    super().speed()
    # do something
```
### override
```python
def speed(self):
    # do something
    pass
```