# Oriented Object general ideas

## Definition
Encaps­ulate what varies.
Code to an interface rather than to an implem­ent­ation.
Each class in your applic­ation should have only one reason to change.
Classes are about behavior and functi­ona­lity.
## Abstraction
The process of separating ideas from specific instances of those ideas at work.
## Class
Simply an abstraction of something
### SuperClass / SubClass
We call SuperClass the parent class. From which class our current class is herited.
We call subclass the inherited classes of our current Class
## Access modifiers
### Private
Only inside the same class instance
### Protected
Inside same or derived class instances
### Public
All other classes linkin­g/r­efe­rencing the class
### Static
Accessible on the class itself (can combine with other accessors)
## Polymorphism
The provision of a single interface to entities of different types. Subtyping.
## Encapsulation
Enclosing objects in a common interface in a way that makes them interc­han­geable, and guards their states from invalid changes
## Inheritance
When an object or class is based on another object or class, using the same implem­ent­ation; it is a mechanism for code reuse. The relati­onships of objects or classes through inheri­tance give rise to a hierarchy.
## Overriding