# Design Patterns

## Definition

## Abstract Factory

## Builder

## Factory Method

## Prototype

## Singleton

## Adapter

## Bridge

## Composite

## Decorator

## Facade

## Flyweight

## Proxy

## Chain of responsability

## Command

## Interpreter

## Iterator

## Mediator

## Memento

## Observer

## State

## Strategy

## Template Method

## Visitor